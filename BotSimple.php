<?php 
class Bot{
	function __construct($fields) {
		$this->server=$fields['server'];
		$this->port=$fields['port'];
		$this->user=$fields['user'];
		$this->pwd=$fields['pwd'];
		$this->chans=$fields['chans'];
		$this->socket=$this->connectBot();
		$this->db=$this->connectDb();
	}
//Connection DB
	public function connectDb(){
		$mysqli = new mysqli('127.0.0.1', 'root', '','botDB');
		if (mysqli_connect_errno()) {
			printf("Échec de la connexion à MySQL: %s\n", mysqli_connect_error());
			exit();
		}
		return $mysqli;
	}	
//Connection
	public function connectBot(){
		(!($socket=fsockopen($this->server,$this->port)))?
		$ret= 'Impossible de se connecter':
		$ret= $socket;
		return $ret;
	}
//Message privé
	public function botPRIVMSG($chan,$msg){
		echo fputs($this->socket,"PRIVMSG $chan :$msg\r\n");
		return fgets($this->socket,1024);
	}
///Action
	public function botACTION($chan,$msg){
		return $this->botPRIVMSG($chan,"\x01ACTION .$msg.\01\r\n");
	}
//Login
	public function loginBot(){
		fputs($this->socket,"USER $this->user $this->user $this->pwd $this->pwd\r\n");
		fputs($this->socket,"NICK $this->user\r\n");
		return fgets($this->socket,1024);
	}
//Join channels
	public function botJoinChan($chan){
		fputs($this->socket,"JOIN $chan\r\n");
		return fgets($this->socket,1024);
	}
//quit IRC
	public function botQuit(){
		fputs($this->socket,"QUIT\r\n");			  		
	}
//Bot routine
	public function botJob(){ 
		$this->loginBot();
		$timeout=1000;
		$donnees='';
		stream_set_timeout($this->socket,1);
		while($timeout>0)
		{	
			($donnees=stream_get_line($this->socket,1028, "\n" ) );
			if($donnees)
			{
				//var_dump($donnees);
				$commande = explode(' ',$donnees);
				$message = explode(':',$donnees);	
				//var_dump($commande);
				//var_dump($message);
						//Ping (avoid irc timeout)
				if($commande[0] == 'PING'){
					fputs($this->socket,"PONG ".$commande[1]."\r\n");
					foreach ($this->chans as $key => $value) {
						$this->botJoinChan($value);
					}
				}
						//Reception
				if(isset($commande[1])&&($commande[1])=='PRIVMSG'){
					$chan=$commande[2];

							//Do you want to quit

					if (isset($message[2])&& (trim($message[2]))=='!quit'){
						$this->botQuit();
						exit;
					}
							//Do you want to slap
					if (isset($message[2])&& (trim($message[2]))=='!slap'){
						$name=trim($message[3])	;
						echo $this->botACTION($chan,"slaps $name with a large trout");
					}
							//Do you want to roll dices
					if (isset($message[2])&& (trim($message[2]))=='!dice'){
						(isset($message[3]))?
						$guess=trim($message[3]):
						$guess='';
						$dice=random_int(1,6);
						$actionMsg="Rolls a dice and gets a $dice .";
						($guess!='')?$actionMsg.=" You bet on $guess and":$actionMsg.="";
						($guess==$dice)?$actionMsg.=" won !":$actionMsg.=" lose.";
						echo $this->botACTION($chan,$actionMsg);
					}
							//Do you want to ask something
					if (isset($message[2])&& (trim($message[2]))=='!q'){
						(isset($message[3]))?
						$question=trim($message[3]):
						$question='';
						if($question=='')
							echo $this->botPRIVMSG($chan,'...   :|');
						else
						{
							echo $question;
							$mysqli=$this->db;
							$stmt = $mysqli->prepare("SELECT quotes.reponses FROM quotes WHERE quotes.questions LIKE (?)");

							$stmt->bind_param('s', $question);
							/* Exécution de la requête */
							if ($result = $stmt->execute()) {
								var_dump($result);
								$response=array();
								/* bind variables to prepared statement */
								$stmt->bind_result($col1);
								/* fetch values */
								if($row=$stmt->fetch()){
									$this->botPRIVMSG($chan , "$col1\n");
									while ($row=$stmt->fetch()) {
										if ($col1!='')
											$this->botPRIVMSG($chan , "$col1\n");
									}
								}else{
									$stmt->close();
									echo $this->botACTION($chan,'is perplex');
									$stmt2 = $mysqli->prepare("INSERT INTO quotes VALUES(?,'')");
									$stmt2->bind_param('s', $question);
									$stmt2->execute();

								}

							}

						}
					}
						//Do you want to add a response ?
					if ((isset($message[2])&& (trim($message[2]))=='!r')){
						
						(isset($message[3]))?
						$response=trim($message[3]):
						$response='';
						if($response=='')
							echo $this->botPRIVMSG($chan,'...   :|');
						else if(isset($question))
						{

							$mysqli=$this->db;
							$stmt = $mysqli->prepare("SELECT quotes.reponses FROM quotes WHERE quotes.questions LIKE (?)");

							$stmt->bind_param('s', $question);

							/* Exécution de la requête */
							if ($result = $stmt->execute()) {
								{$stmt->close();

									$stmt2 = $mysqli->prepare("UPDATE quotes SET reponses=? WHERE questions LIKE ?");
									$stmt2->bind_param('ss', $response,$question);
									$stmt2->execute();
									echo $this->botACTION($chan,"is learning  that $question answer is $response");
								}
							}
						}
						unset($question);
					}
				}
			}
				///Check if 'human written' messages are in the file to send it to irc.
			($postHuman=$this->checkPostMsgQueued())?
			$this->botPRIVMSG($chan,trim($postHuman)):
			$postHuman=0;
			$timeout-=1;
				//Flush data to display
			ob_flush();
			flush();
				//TODO check if useful...
			usleep(300);
		}
		//quit
		$this->botQuit();
	}
	public function checkPostMsgQueued(){
		//please format like this : #chan $msg
		$filename=".\PRVIMSG_QUEUE.txt";
		if (file_exists($filename)){
			$stream=fopen($filename, 'rw');
			$res=stream_get_contents($stream,1024);
			file_put_contents($filename, "");
			echo $res;
			return $res;
		}
		return 0;
	}
};
?>