# README #

This is a simple PHP IRC Bot, with basic/funny features.

You may need : 

PHP (7.0.10)
MySQL (5.7.14)

"Features" : 

-Connection DB
-Connection IRC
-Login
-JOIN
-QUIT
-PRIVMSG
-ACTION

Job routine to execute commands : 
-ping
-quit
-slap
-dice
-q (question)
-r (response)
-human message queuing

### Who do I talk to? ###

Contact : franck.jude@free.fr